# Benchmarking Xsuite and MAD-X Slow Extraction

<a href="https://swan-k8s.cern.ch/user-redirect/download?projurl=https%3A%2F%2Fgitlab.cern.ch%2Ftbass%2Fxsuite-benchmarking-demo.git"><img class="open_in_swan" data-path="your_submodule_name" alt="Open this Gallery in SWAN" src="https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png"></a>


This notebook contains a basic Slow Extraction simulation using the SPS machine, using the lattice files found on the [acc-models-sps repo](https://acc-models.web.cern.ch/acc-models/sps/) 

It has been written in both MAD-X (`madx_track()`) and Xsuite/Xtrack (`xtrack_track()`).

In the final cell, you will see an example of how to run a simple benchmarking simulation using `run()`. `run()` takes three arguments:
- The number of particles to generate for the initial particle distribution
- The number of turns to simulate for
- Either the string `"CPU"`, or `"GPU"` if you want to use CuPy/CUDA GPGPU acceleration

`run()` will then save the time the simulations took to `results.csv` (which has been pre-populated with the header row), and produce matplotlib graphs of the phase-space of the final turn.

## Requirements

- [Xsuite](https://xsuite.readthedocs.io/en/latest/installation.html)
- [MAD-X](https://mad.web.cern.ch/mad/) and [cpymad](https://hibtc.github.io/cpymad/installation.html)
- numPy, matplotlib, pandas
- [CuPy](https://docs.cupy.dev/en/stable/install.html)
- [Henontrack](https://gitlab.cern.ch/parrutia/henontrack)
- [PyBT](https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/pybt)

## Running on SWAN

This notebook is designed to run on SWAN with GPU acceleration. 

First, request to be added to the e-group for SWAN GPU use [here](https://cern.service-now.com/service-portal?id=functional_element&name=swan).

Then, visit https://swan-k8s.cern.ch and spawn a session with `102 Cuda 11.2 (GPU)`. This will assign your Jupyter session a Tesla T4 GPU.